<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
//$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// =============== Begin Auth ===============
$routes->get('/', 'Auth::index');
$routes->post('/login', 'Auth::login_proses');
$routes->get('/register', 'Auth::register');
$routes->post('/register', 'Auth::register_proses');
$routes->get('/forgot-password', 'Auth::forgot_password');
$routes->post('/forgot-password', 'Auth::forgot_proses');
$routes->get('/reset-password', 'Auth::reset_password');
$routes->post('/reset-password', 'Auth::reset_proses');
$routes->get('/logout', 'Auth::logout');
// ================ End Auth ================

// =============== Begin Dashboard ===============
$routes->group('dashboard', static function ($routes) {
    $routes->get('', 'Dashboard::index');
    $routes->post('get-data', 'Dashboard::get_data');
    $routes->get('get-device', 'Dashboard::get_device');
    $routes->get('get-data-grafik', 'Dashboard::get_data_grafik');
});
// ================ End Dashboard ================

// =============== Begin User ===============
$routes->group('ms-user', static function ($routes) {
    $routes->get('', 'Ms_user::index');
    $routes->post('', 'Ms_user::save');
    $routes->delete('(:any)', 'Ms_user::hapus/$1');
    $routes->post('get_data', 'Ms_user::get_data');
    $routes->get('akses', 'Ms_user::get_akses');
    $routes->post('akses', 'Ms_user::save_akses');
});
// ================ End User ================

// =============== Begin Group ===============
$routes->group('ms-group', static function ($routes) {
    $routes->get('', 'Ms_group::index');
    $routes->post('', 'Ms_group::save');
    $routes->delete('(:any)', 'Ms_group::hapus/$1');
    $routes->post('get-data', 'Ms_group::get_data');
    $routes->get('akses', 'Ms_group::get_menu');
    $routes->post('akses', 'Ms_group::save_akses');
});
// ================ End Group ================

// =============== Begin Menu ===============
$routes->group('ms-menu', static function ($routes) {
    $routes->get('', 'Ms_menu::index');
    $routes->post('', 'Ms_menu::save');
    $routes->delete('(:any)', 'Ms_menu::hapus/$1');
    $routes->post('get-data', 'Ms_menu::get_data');
    $routes->get('parent', 'Ms_menu::get_parent');
});
// ================ End Menu ================

// =============== Begin Setting ===============
$routes->group('setting', static function ($routes) {
    $routes->get('', 'Setting::index');
    $routes->post('', 'Setting::save');
    $routes->post('get-data', 'Setting::get_data');
});
// ================ End Setting ================

// =============== Begin Ms Device ===============
$routes->group('ms-device', static function ($routes) {
    $routes->get('', 'Ms_device::index');
    $routes->post('', 'Ms_device::save');
    $routes->delete('(:any)', 'Ms_device::hapus/$1');
    $routes->post('get-data', 'Ms_device::get_data');
});
// ================ End Ms Device ================

// =============== Begin List Device ===============
$routes->group('devices', static function ($routes) {
    $routes->get('', 'List_device::index');
    $routes->post('', 'List_device::save');
    $routes->delete('(:any)', 'List_device::hapus/$1');
    $routes->post('get-data', 'List_device::get_data');
});
// ================ End List Device ================

// =============== Begin API ===============
$routes->group('api', static function ($routes) {
    $routes->post('data', 'Data_device::store');
});
// ================ End API ================

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
