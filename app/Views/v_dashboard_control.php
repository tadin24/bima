<h1 class="mt-4"><?= $title ?></h1>
<div class="row" id="rowList">
  <div class="col-xl-12">
    <div class="card mb-4">
      <div class="card-header text-white bg-secondary">
        <div class="row">
          <div class="col-sm-4">
            <h4 class="card-title">
              Data <?= $title ?>
            </h4>
          </div>
          <div class="col-sm-8">
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row mb-3">
          <div class="col-md-6">
            <div class="row">
              <label class="col-md-3 col-form-label">Jenis Device</label>
              <div class="col-md-5">
                <select class="form-control" id="device_id" name="device_id">
                  <?php foreach ($opt_ms_device as $v) : ?>
                    <option value="<?= $v->device_id ?>"><?= $v->device_nama ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <label class="col-md-3 col-form-label">Nama Device</label>
              <div class="col-md-5">
                <select class="form-control" id="ld_id" name="ld_id">
                  <option value="0">Pilih Nama Device</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-sm-6">
            <div class="row mb-2 text-center">
              <div class="col-12">
                <button class="btn btn-primary aksi" value="A"><i class="fa fa-caret-up"></i></button>
              </div>
            </div>
            <div class="row mb-2">
              <div class="col-6 text-center">
                <button class="btn btn-primary aksi" value="D"><i class="fa fa-caret-left"></i></button>
              </div>
              <div class="col-6 text-center">
                <button class="btn btn-primary aksi" value="B"><i class="fa fa-caret-right"></i></button>
              </div>
            </div>
            <div class="row mb-2 text-center">
              <div class="col-12">
                <button class="btn btn-primary aksi" value="C"><i class="fa fa-caret-down"></i></button>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mb-2">
              <div class="col-6 text-center">
                <button class="btn btn-primary aksi" value="E"><i class="fa fa-stop"></i></button>
              </div>
              <div class="col-6 text-center">
                <button class="btn btn-primary aksi" value="F"><i class="fa fa-redo"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function getListDevice() {
    const device_id = $("#device_id").val()
    $.ajax({
      url: '<?= base_url() ?>/dashboard/get_device',
      data: {
        device_id: device_id
      },
      cache: false,
      dataType: 'json',
      success: res => {
        $("#ld_id option[value!=0]").remove()
        if (res.length > 0) {
          let opt = ''
          $.each(res, function(index, item) {
            opt += `<option value="${item.ld_id}" data-url="${item.ld_url}">${item.ld_kode}</option>`
          })

          $("#ld_id").append(opt)
        }
      }
    })
  }

  $(document).ready(function() {
    $("#device_id").change(function() {
      getListDevice();
    })

    $("#device_id").trigger('change')

    $('.aksi').click(function() {
      const ldId = $("#ld_id")
      if (ldId.val() > 0) {
        const url = ldId.find('option:selected').data('url')
        console.log(url);
        $.ajax({
          url: url,
          data: {
            value: $(this).val()
          },
          cache: false,
          type: 'post',
          dataType: 'json',
          success: res => {
            console.log(res);
          }
        })
      } else {
        Swal.fire({
          icon: "error",
          title: "Error",
          html: "Pilih Device terlebih dahulu",
        })
      }
    })
  })
</script>