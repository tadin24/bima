<h1 class="mt-4"><?= $title ?></h1>
<div class="row" id="rowList">
  <div class="col-xl-12">
    <div class="card mb-4">
      <div class="card-header text-white bg-secondary">
        <div class="row">
          <div class="col-sm-4">
            <h4 class="card-title">
              Data <?= $title ?>
            </h4>
          </div>
          <div class="col-sm-8">
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row mb-3">
          <div class="col-md-6">
            <div class="row">
              <label class="col-md-3 col-form-label">Jenis Device</label>
              <div class="col-md-5">
                <select class="form-control" id="device_id" name="device_id">
                  <?php foreach ($opt_ms_device as $v) : ?>
                    <option value="<?= $v->device_id ?>"><?= $v->device_nama ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <label class="col-md-3 col-form-label">Nama Device</label>
              <div class="col-md-5">
                <select class="form-control" id="ld_id" name="ld_id">
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-6">
            <label class="col-form-label">DO</label>
            <canvas id="doChart"></canvas>
          </div>
          <div class="col-md-6">
            <label class="col-form-label">Ph</label>
            <canvas id="phChart"></canvas>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-6">
            <label class="col-form-label">Suhu</label>
            <canvas id="suhuChart"></canvas>
          </div>
          <div class="col-md-6">
            <label class="col-form-label">Turbidity</label>
            <canvas id="turbidityChart"></canvas>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-6">
            <label class="col-form-label">TDS</label>
            <canvas id="salinitasChart"></canvas>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-12">
                <label class="col-form-label">Klasifikasi</label>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <span style="font-size: 40px;" class="badge rounded-pill bg-success" id="label_klasifikasi">Aman</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-9">
            <div class="row">
              <label class="col-md-3 col-form-label">Periode</label>
              <div class="col-md-9">
                <div class="input-group">
                  <input type="text" readonly class="form-control tgl" id="tgl_mulai" name="tgl_mulai" value="<?= date('d-m-Y') ?>">
                  <span class="input-group-text">s/d</span>
                  <input type="text" readonly class="form-control tgl" id="tgl_selesai" name="tgl_selesai" value="<?= date('d-m-Y') ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-12">
            <table class="table table-hover table-bordered table-striped" id="tbl_vendor">
              <thead>
                <tr>
                  <th class="text-center">No.</th>
                  <th class="text-center">Created At</th>
                  <th class="text-center">DO</th>
                  <th class="text-center">pH</th>
                  <th class="text-center">Suhu</th>
                  <th class="text-center">Turbidity</th>
                  <th class="text-center">TDS</th>
                  <th class="text-center">Klasifikasi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  let doChart, phChart, suhuChart, turbidityChart, salinitasChart, intervalDashboard, typeGet = 1,
    lastId = 0,
    panjangData = 0;

  const PageAdvanced = function() {

    const initTableLogger = function() {
      var table = $('#tbl_vendor');

      // begin first table
      table.DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        ajax: {
          url: '<?= base_url() ?>/dashboard/get-data',
          type: 'POST',
          data: function(d) {
            d.ld_id = $("#ld_id").val();
            d.tgl_mulai = $("#tgl_mulai").val();
            d.tgl_selesai = $("#tgl_selesai").val();
          },
        },
        columnDefs: [{
          targets: [0],
          orderable: false,
        }, {
          targets: [0],
          className: 'text-center',
        }, ],
        "order": [
          [1, 'desc']
        ]
      });
    };

    const initLineChart = () => {
      const generateGrafik = (id, judul = '', warna = '#000') => {
        return new Chart(
          document.getElementById(id), {
            type: 'line',
            data: {
              labels: [],
              datasets: [{
                label: judul,
                data: [],
                fill: false,
                borderColor: warna,
                tension: 0.1
              }]
            },
          }
        );
      }

      doChart = generateGrafik('doChart', 'DO Chart', 'rgb(75, 192, 192)')
      phChart = generateGrafik('phChart', 'Ph Chart', 'rgb(75, 192, 100)')
      suhuChart = generateGrafik('suhuChart', 'Suhu Chart', 'rgb(100, 192, 192)')
      salinitasChart = generateGrafik('salinitasChart', 'TDS Chart', 'rgb(75, 20, 100)')
      turbidityChart = generateGrafik('turbidityChart', 'Turbidity Chart', 'rgb(255, 192, 100)')
    }

    return {

      //main function to initiate the module
      init: function() {
        initTableLogger();
        initLineChart();
      },

    };

  }();

  function getListDevice() {
    const device_id = $("#device_id").val()
    $.ajax({
      url: '<?= base_url() ?>/dashboard/get-device',
      data: {
        device_id: device_id
      },
      cache: false,
      dataType: 'json',
      success: res => {
        if (res.length > 0) {
          let opt = ''
          $.each(res, function(index, item) {
            opt += `<option value="${item.ld_id}">${item.ld_kode}</option>`
          })

          $("#ld_id").html(opt)
          $("#ld_id").trigger('change')
        }
      }
    })
  }

  function getDataGrafik() {
    $.ajax({
      url: '<?= base_url() ?>/dashboard/get-data-grafik',
      cache: false,
      data: {
        ld_id: $("#ld_id").val(),
        type_get: typeGet,
        last_id: lastId,
      },
      dataType: 'json',
      success: res => {
        const item = res.data;

        if (typeGet == 1) {
          doChart.data.datasets[0].data = item.do;
          doChart.data.labels = item.labels;
          doChart.update();

          phChart.data.datasets[0].data = item.ph;
          phChart.data.labels = item.labels;
          phChart.update();

          suhuChart.data.datasets[0].data = item.suhu;
          suhuChart.data.labels = item.labels;
          suhuChart.update();

          turbidityChart.data.datasets[0].data = item.turbidity;
          turbidityChart.data.labels = item.labels;
          turbidityChart.update();

          salinitasChart.data.datasets[0].data = item.salinitas;
          salinitasChart.data.labels = item.labels;
          salinitasChart.update();

          panjangData = item.labels.length
        } else {

          if (res.last_id != lastId) {
            if (panjangData >= 10) {
              doChart.data.datasets[0].data.shift();
              doChart.data.labels.shift();

              phChart.data.datasets[0].data.shift();
              // phChart.data.labels.shift();

              suhuChart.data.datasets[0].data.shift();
              // suhuChart.data.labels.shift();

              turbidityChart.data.datasets[0].data.shift();
              // turbidityChart.data.labels.shift();

              salinitasChart.data.datasets[0].data.shift();
              // salinitasChart.data.labels.shift();
            }

            doChart.data.datasets[0].data.push(item.do);
            doChart.data.labels.push(item.labels);

            phChart.data.datasets[0].data.push(item.ph);
            // phChart.data.labels.push(item.labels);

            suhuChart.data.datasets[0].data.push(item.suhu);
            // suhuChart.data.labels.push(item.labels);

            turbidityChart.data.datasets[0].data.push(item.turbidity);
            // turbidityChart.data.labels.push(item.labels);

            salinitasChart.data.datasets[0].data.push(item.salinitas);
            // salinitasChart.data.labels.push(item.labels);

            if (panjangData < 10) panjangData++;
            doChart.update();
            phChart.update();
            suhuChart.update();
            turbidityChart.update();
            salinitasChart.update();

            // update lastId
            lastId = res.last_id
          }
        }

        typeGet = 0;
        lastId = res.last_id;

        if (item.klasifikasi.toLowerCase() == "aman") {
          $("#label_klasifikasi").attr("class", "badge rounded-pill bg-success");
        } else if (item.klasifikasi.toLowerCase() == "siaga") {
          $("#label_klasifikasi").attr("class", "badge rounded-pill bg-warning");
        } else if (item.klasifikasi.toLowerCase() == "bahaya") {
          $("#label_klasifikasi").attr("class", "badge rounded-pill bg-danger");
        } else {
          $("#label_klasifikasi").attr("class", "badge rounded-pill bg-secondary");
        }
        $("#label_klasifikasi").text(item.klasifikasi);
      }
    })
  }

  const reload_tbl = () => {
    $("#tbl_vendor").dataTable().fnDraw();
  }

  const updateData = () => {
    getDataGrafik();
    reload_tbl();
  }

  $(document).ready(function() {
    PageAdvanced.init();
    $("#device_id").change(function() {
      getListDevice();
    })

    $("#device_id").trigger('change')

    $('.tgl').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
    })

    $('#tgl_mulai,#tgl_selesai').change(function() {
      reload_tbl();
    })

    $("#ld_id").change(function() {
      typeGet = 1;

      clearInterval(intervalDashboard);
      intervalDashboard = setInterval(() => {
        updateData()
      }, 5000);
    })

  })
</script>