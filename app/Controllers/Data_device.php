<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\M_data_device;
use Config\Database;

class Data_device extends BaseController
{
    protected $ld_id;
    protected $M_data_device;

    public function __construct()
    {
        $this->M_data_device = new M_data_device();
    }

    public function check()
    {
        $device_kode = $this->request->getVar('device_kode');
        $db = Database::connect();
        $device = $db
            ->query(
                "SELECT
                    *
                from
                    list_device ld
                where
                    ld.ld_kode = '$device_kode'"
            );
        if ($device->getNumRows() <= 0) {
            $res['status'] = false;
            $res['data'] = $this->response->setStatusCode(403)->setJSON(['msg' => 'Device tidak ditemukan!']);
        } else {
            if ($device->getRow()->ld_status != 1) {
                $res['status'] = false;
                $res['data'] = $this->response->setStatusCode(403)->setJSON(['msg' => 'Device tidak aktif!']);
            } else {
                $this->ld_id = $device->getRow()->ld_id;
                $res['status'] = true;
            }
        }

        return $res;
    }

    // public function store()
    // {
    //     $db = Database::connect();
    //     $db->query(
    //             "INSERT into tes (input)
    //             VALUES
    //             ('". json_encode($_REQUEST) ."');"
    //         );

    //         return $this->response->setStatusCode(200)->setJSON(['msg' => 'Data berhasil diinsert!']);
    // }

    public function store()
    {
        $res = $this->check();
        if (!$res['status']) {
            return $res['data'];
        }

        $data = [
            'ld_id' => $this->ld_id,
            'do' => $this->request->getVar('do'),
            'ph' => $this->request->getVar('ph'),
            'suhu' => $this->request->getVar('suhu'),
            'turbidity' => $this->request->getVar('turbidity'),
            'salinitas' => $this->request->getVar('salinitas'),
            'klasifikasi' => $this->request->getVar('klasifikasi'),
        ];

        $res = $this->M_data_device->save($data);

        if ($res) {
            return $this->response->setStatusCode(200)->setJSON(['msg' => 'Data berhasil ditambahkan!']);
        } else {
            return $this->response->setStatusCode(400)->setJSON(['msg' => 'Data gagal ditambahkan!']);
        }
    }
}
