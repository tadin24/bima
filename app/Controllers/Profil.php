<?php

namespace App\Controllers;

use App\Controllers\MyController;

class Profil extends MyController
{
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }

    public function index($id)
    {
        $data['user'] = $this->db->table('ms_user')->where('user_id', $id)->get()->getRow();
        $data['title'] = "Profil";
        return $this->base_theme('v_profil', $data);
    }

    public function update_pass()
    {
        $password = password_hash($this->request->getVar('password'), PASSWORD_DEFAULT);

        $data = [
            'password' => $password,
        ];

        $id = $this->request->getVar('user_id');
        $res = $this->db->table('ms_user')->where('user_id', $id)->update($data);

        if ($res > 0) {
            $response = [
                'status' => true,
                'message' => 'Berhasil memperbarui password!',
                'title' => 'Success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Gagal memperbarui password!',
                'title' => 'Error',
            ];
        }

        echo json_encode($response);
    }
}
