<?php

namespace App\Controllers;

use App\Controllers\MyController;
use App\Models\M_ms_device;

class Dashboard_control extends MyController
{
    protected $M_ms_device;

    public function __construct()
    {
        parent::__construct();
        $this->M_ms_device = new M_ms_device();
    }

    public function index()
    {
        $data['opt_ms_device'] = $this->M_ms_device
            ->where('device_status', 1)
            ->orderBy('device_kode', 'asc')
            ->find();
        $data['title'] = "Dashboard Kontrol";
        return $this->base_theme('v_dashboard_control', $data);
    }
}
