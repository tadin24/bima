<?php

namespace App\Models;

use CodeIgniter\Model;

class M_dashboard extends Model
{
    public function get_total($where)
    {
        $sql = "SELECT
                    count(*) as total
                from
                    data_device dd
                where
                    0 = 0
                    $where";
        return $this->db->query($sql)->getRow()->total;
    }

    public function get_data($limit, $where, $order, $columns)
    {
        $slc = implode(',', $columns);
        $sql = "SELECT
                    $slc
                from
                    data_device dd
                where
                    0 = 0
                    $where
                $order $limit";
        return $this->db->query($sql)->getResult();
    }

    public function get_sensor($where, $limit)
    {
        $sql = "SELECT
                    *
                from
                    (
                    select
                        dd.dd_id,
                        dd.do,
                        dd.ph,
                        dd.suhu,
                        dd.turbidity,
                        dd.salinitas,
                        dd.klasifikasi,
                        dd.created_at
                    from
                        data_device dd
                    where
                        0 = 0
                        $where
                    order by
                        dd_id desc
                    limit $limit) d
                order by
                    d.dd_id";
        // echo $sql;
        return $this->db->query($sql)->getResult();
    }

    public function get_label($where, $limit)
    {
        $sql = "SELECT
                    *
                from
                    (
                    select
                        dd.created_at,
                        dd.dd_id
                    from
                        data_device dd
                    where
                        0 = 0
                        $where
                    order by
                        dd.dd_id desc
                    limit $limit) d
                order by
                    d.created_at;";
        return $this->db->query($sql)->getResult();
    }
}
