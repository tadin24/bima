<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MsDeviceSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'device_kode' => "01",
                'device_nama' => "Autofeeder",
                'device_status' => 1,
            ], [
                'device_kode' => "02",
                'device_nama' => "Aerator",
                'device_status' => 1,
            ],
        ];

        $this->db->table("ms_device")->insertBatch($data);
    }
}
